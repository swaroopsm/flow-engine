export function generateSimpleRules() {
  // Assuming these to be a normalized, else use normalizr if server returns an array of rules
  return {
    1: {
      id: 1,
      title: 'Value Exists',
      body: (arg) => {
        return !!arg;
      },
      true_id: 2,
      false_id: 3,
    },
    2: {
      id: 2,
      title: 'Is a Number',
      body: (arg) => {
        return typeof arg === 'number';
      },
      true_id: 4,
      false_id: null,
    },
    3: {
      id: 3,
      title: 'Mock True',
      body: (arg) => {
        return true;
      },
      true_id: null,
      false_id: null,
    },
    4: {
      id: 4,
      title: 'Number is Even',
      body: (arg) => {
        return arg % 2 === 0;
      },
      true_id: null,
      false_id: 5,
    },
    5: {
      id: 5,
      title: 'Number is Odd',
      body: (arg) => {
        return true;
      },
      true_id: null,
      false_id: null,
    },
  };
}

export function fetchRules() {
  return generateSimpleRules();
}

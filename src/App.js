import React, { Component } from 'react';
import logo from './logo.svg';
import styles from './App.css';
import { fetchRules } from 'services/rule';
import RuleList from 'components/RuleList';
import runFlow from 'helpers/runFlow';
import Input from 'components/Input';

class App extends Component {
  state = {
    rules: [],
    appError: false,
    input: 4,
  };

  componentDidMount() {
    this.runFlow(this.state.input);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.input !== this.state.input) {
      this.runFlow(this.state.input);
    }
  }

  runFlow(input) {
    const rules = fetchRules();
    const parsedValue = parseInt(input, 10);
    const value = isNaN(parsedValue) ? input : parsedValue;

    runFlow(rules, { input: value })
      .then((data) => {
        const rules = (
          Object.keys(data)
            .map((ruleId) => (data[ruleId]))
        );

        this.setState({ rules });
      })
      .catch((e) => {
        this.setState({ appError: e.message })
      });
  }

  handleInputChange = (e) => {
    this.setState({ input: e.target.value });
  }

  render() {
    return (
      <div className={styles.app}>
        <header className={styles.appHeader}>
          <img src={logo} className={styles.appLogo} alt="logo" />
          <h1 className={styles.appTitle}>Flow Engine</h1>
        </header>
        <p className={styles.appIntro}>
        </p>
        <div className={styles.appContainer}>
          <Input
            value={this.state.input}
            onChange={this.handleInputChange}
          />
          {
            this.state.appError
            ? (
              <div className={styles.appError}>Error: {this.state.appError}</div>
            )
            : (
              <RuleList
                rules={this.state.rules}
              />
            )
          }
        </div>
      </div>
    );
  }
}

export default App;

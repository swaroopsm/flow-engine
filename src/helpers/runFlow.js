function isCircularList(rules, { id }) {
  return !!rules[id];
}

function evalFn(rules, node, arg, result = {}) {
  if (isCircularList(result, node)) {
    throw new Error('List is circular');
  }

  const evaluated = node.body(arg);

  if (evaluated && node.true_id)  {
    return evalFn(rules, rules[node.true_id], arg, { ...result, [node.id]: { ...node, body: node.body.toString(), success: true } });
  }

  if (!evaluated && node.false_id)  {
    return evalFn(rules, rules[node.false_id], arg, { ...result, [node.id]: { ...node, body: node.body.toString(), success: false } });
  }

  return { ...result, [node.id]: { ...node, body: node.body.toString(), success: evaluated } };
}

function runFlow(rules, { input }) {
  const promise = new Promise((resolve, reject) => {
    try {
      const head = rules[Object.keys(rules)[0]];
      const result = evalFn(rules, head, input);

      resolve(result);
    } catch (e) {
      reject(e);
    }
  });

  return promise;
}

export default runFlow;

import React from 'react';
import styles from './styles.css';

export default function Input({ value, onChange }) {
  return (
    <div className={styles.wrapper}>
      <input
        value={value}
        onChange={onChange}
        className={styles.input}
        placeholder="Enter Input"
      />
    </div>
  )
}

import React from 'react';
import RuleItem from 'components/RuleItem';
import styles from './styles.css';

class RuleList extends React.PureComponent {
  render() {
    return (
      <div className={styles.wrapper}>
      {
        this.props.rules.map((rule) => (
          <RuleItem
            key={rule.id}
            className={styles.ruleItem}
            rule={rule}
          />
        ))
      }
      </div>
    );
  }
}

export default RuleList;

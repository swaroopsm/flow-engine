import React from 'react';
import styles from './styles.css'

function RuleDefinition({ definitionTerm, children }) {
  return (
    <div className={styles.wrapper}>
      <span className={styles.ruleDefnTerm}>{definitionTerm}</span>
      <div className={styles.ruleDefnBody}>
        {children}
      </div>
    </div>
  )
}

export default RuleDefinition;

import React from 'react';
import PropTypes from 'prop-types'
import styles from './styles.css';
import { Col, Row } from 'react-flexbox-grid';
import RuleDefinition from 'components/RuleDefinition';
import classNames from 'classnames';

class RuleItem extends React.PureComponent {
  static propTypes = {
    rule: PropTypes.object,
    className: PropTypes.string,
  };

  state = {
    isOpen: false,
  };

  handleClick = () => {
    this.setState({ isOpen: !this.state.isOpen });
  }

  render() {
    const { rule, className} = this.props;
    const { success} = rule;

    return (
      <div className={classNames(styles.wrapper, className)}>
        <Row className={styles.ruleHeader} onClick={this.handleClick}>
          <Col sm={2} className={classNames(styles.ruleId, {[styles.ruleIdSuccess]: success, [styles.ruleIdError]: !success})}>
            <span>{rule.id}</span>
          </Col>
          <Col sm={10} className={classNames(styles.ruleTitle, {[styles.ruleTitleSuccess]: success, [styles.ruleTitleError]: !success})}>
            <span>{rule.title}</span>
          </Col>
        </Row>
        {
          this.state.isOpen &&
          <div className={styles.ruleBody}>
            <Row>
              <Col sm={12}>
                <RuleDefinition definitionTerm="Rule body">
                  {rule.body}
                </RuleDefinition>
              </Col>
            </Row>
            <Row>
              <Col sm={6}>
                <RuleDefinition definitionTerm="Next rule-id if passed">
                  {rule.true_id || '-'}
                </RuleDefinition>
              </Col>
              <Col sm={6}>
                <RuleDefinition definitionTerm="Next rule-id if failed">
                  {rule.false_id || '-'}
                </RuleDefinition>
              </Col>
            </Row>
          </div>
        }
      </div>
    );
  }
}

export default RuleItem;
